package pFinalTema7;

import javax.swing.JOptionPane;

public class Motocicleta extends Vehiculo implements PuedeCircular {

	private int potenciaMotor;
	private boolean tieneMaletero;
	
	public Motocicleta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Motocicleta(int potenciaMotor, boolean tieneMaletero) {
		super();
		this.potenciaMotor = potenciaMotor;
		this.tieneMaletero = tieneMaletero;
	}
	@Override
	public String toString(){
		
		String caracteristicas = super.toString();
		
		caracteristicas = caracteristicas + "La potencia del motor es: " + potenciaMotor +"\n";
		
		if (tieneMaletero){
			caracteristicas = caracteristicas + "La motocicleta tiene maletero\n";
		}
		else{
			caracteristicas = caracteristicas + "La motocicleta no tiene maletero\n";
		}
						
		return caracteristicas;
		
	}
	
	public int getPotenciaMotor() {
		return potenciaMotor;
	}
	public void setPotenciaMotor(int potenciaMotor) {
		this.potenciaMotor = potenciaMotor;
	}
	public boolean isTieneMaletero() {
		return tieneMaletero;
	}
	public void setTieneMaletero(boolean tieneMaletero) {
		this.tieneMaletero = tieneMaletero;
	}
	public void aparcar(){
		JOptionPane.showMessageDialog(null,"La motocicleta est� aparcada.");
	}
	public void brincar(){
		JOptionPane.showMessageDialog(null,"La motocicleta est� brincando.");	
	}
	@Override
	public void circular() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"Esto es una motocicleta y las motocicletas pueden circular por carreteras, autov�as y autopistas.");
	}
	@Override
	public void arrancar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La motocicleta ha arrancado.");
	}
	@Override
	public void acelerar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La motocicleta ha acelerado.");
	}
	@Override
	public void frenar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La motocicleta ha frenado.");
	}
}
