package pFinalTema7;

import javax.swing.JOptionPane;

public class Yate extends Vehiculo implements PuedeNavegar {

	private boolean tieneCocina;
	private int numMotores;
	private int metrosEslora;
		
	public Yate() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Yate(boolean tieneCocina, int numMotores, int metrosEslora) {
		super();
		this.tieneCocina = tieneCocina;
		this.numMotores = numMotores;
		this.metrosEslora = metrosEslora;
	}
	@Override
	public String toString(){
		
		String caracteristicas = super.toString();
		
		caracteristicas = caracteristicas + "El numero de motores del yate es: " + numMotores +"\n";
		caracteristicas = caracteristicas + "Los metros de eslora del yate son: " + metrosEslora +"\n";
		
		if (tieneCocina){
			caracteristicas = caracteristicas + "El yate tiene cocina\n";
		}
		else{
			caracteristicas = caracteristicas + "El yate no tiene cocina\n";
		}
						
		return caracteristicas;
		
	}
	
	public boolean isTieneCocina() {
		return tieneCocina;
	}
	public void setTieneCocina(boolean tieneCocina) {
		this.tieneCocina = tieneCocina;
	}
	public int getNumMotores() {
		return numMotores;
	}
	public void setNumMotores(int numMotores) {
		this.numMotores = numMotores;
	}
	public int getMetrosEslora() {
		return metrosEslora;
	}
	public void setMetrosEslora(int metrosEslora) {
		this.metrosEslora = metrosEslora;
	}
	public void zarpar(){
		JOptionPane.showMessageDialog(null,"El yate est� zarpando.");
	}
	public void atracar(){
		JOptionPane.showMessageDialog(null,"El yate est� atracando.");
	}
	@Override
	public void navegar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El yate est� navegando.");
	}
	@Override
	public void arrancar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El yate ha arrancado.");
	}
	@Override
	public void acelerar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El yate ha acelerado.");
	}
	@Override
	public void frenar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El yate ha frenado.");
	}
}
