package pFinalTema7;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Parque {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String opcionString;
		int opcion;
		String opcionString2;
		int opcion2;
		boolean repetir = true;
		boolean repetir2 = true;
		ArrayList<Vehiculo> Vehiculos = new ArrayList<Vehiculo>(); 
		Vehiculo auxiliar;
		
		do {
				String menu = "Parque de Veh�culos de Carlos J. Segu�. Selecciona una de las siguientes opciones:\n";
				menu = menu + "1. Fabricar coche\n";
				menu = menu + "2. Fabricar autob�s\n";
				menu = menu + "3. Fabricar motocicleta\n";
				menu = menu + "4. Fabricar avioneta\n";
				menu = menu + "5. Fabricar yate\n";
				menu = menu + "6. Mostrar caracter�sticas\n";
				menu = menu + "7. Salir del programa\n";
				
				try {
					opcionString = JOptionPane.showInputDialog(menu);

					opcion = Integer.parseInt(opcionString);
				}
				catch (Exception E) {
					opcionString = "La opci�n elegida ha de ser num�rica";
					JOptionPane.showMessageDialog(null, opcionString);
					repetir = true;
					opcion = -1;
				}
				
		
				switch (opcion){
				
				case 1: 
					
					if(Vehiculo.getNumVehiculos()>(Vehiculo.maxVehiculos -1)){
						JOptionPane.showMessageDialog(null,"Parque lleno, no se pueden crear m�s veh�culos.");
					}else{
						
						do{
							String menu2 = "Seleccione opci�n para crear coche:\n";
							menu2 = menu2 + "1. Fabricaci�n autom�tica\n";
							menu2 = menu2 + "2. Fabricaci�n indicando airbags, techo solar y radio\n";
							menu2 = menu2 + "3. Atras\n";
							
							try {
								opcionString2 = JOptionPane.showInputDialog(menu2);

								opcion2 = Integer.parseInt(opcionString2);
							}
							catch (Exception E) {
								opcionString2 = "La opci�n elegida ha de ser num�rica";
								JOptionPane.showMessageDialog(null, opcionString2);
								repetir2 = true;
								opcion2 = -1;
							}
							
							switch (opcion2){
							
							case 1: 
													
								Vehiculos.add(new Coche());
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								break;
								
							case 2: 
								
								int numAirbags=0;
								boolean tieneTechoSolar = false;
								boolean tieneRadio = false;
								
								try {
									
									numAirbags = Integer.parseInt(JOptionPane.showInputDialog("Introduce n�mero de airbags:"));
									while (numAirbags > 5){
										JOptionPane.showMessageDialog(null,"El valor m�ximo es 5");
										numAirbags = Integer.parseInt(JOptionPane.showInputDialog("Introduce n�mero de airbags:"));
									}
																	}
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
									
									int techoSolar = Integer.parseInt(JOptionPane.showInputDialog("Tiene techo solar?:\n 1.SI\n 2.NO "));
										if(techoSolar == 1){
											
											tieneTechoSolar = true;	
										
										}
										else if (techoSolar == 2){
										
											tieneTechoSolar = false;
											
										}
										
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
								
									int radio = Integer.parseInt(JOptionPane.showInputDialog("Tiene radio?:\n 1.SI\n 2.NO "));
										if(radio == 1){
											
											tieneRadio = true;	
										
										}
										else if (radio == 2){
										
											tieneRadio = false;
										}
										
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								Vehiculos.add(new Coche(numAirbags,tieneTechoSolar,tieneRadio));
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								
								break;
							
							case 3: 
								
								repetir2 = false;
								break;
							}
						}while (repetir2);
								
					}
					break;
				
				case 2: 
									
					if(Vehiculo.getNumVehiculos()>(Vehiculo.maxVehiculos -1)){
						JOptionPane.showMessageDialog(null,"Parque lleno, no se pueden crear m�s veh�culos.");
					}else{
						
						do{
							String menu2 = "Seleccione opci�n para crear autobus:\n";
							menu2 = menu2 + "1. Fabricaci�n autom�tica\n";
							menu2 = menu2 + "2. Fabricaci�n indicando tipo de recorrido y si es escolar\n";
							menu2 = menu2 + "3. Atras\n";
							
							try {
								opcionString2 = JOptionPane.showInputDialog(menu2);

								opcion2 = Integer.parseInt(opcionString2);
							}
							catch (Exception E) {
								opcionString2 = "La opci�n elegida ha de ser num�rica";
								JOptionPane.showMessageDialog(null, opcionString2);
								repetir2 = true;
								opcion2 = -1;
							}
							
							switch (opcion2){
							
							case 1: 
													
								Vehiculos.add(new Autobus());
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								break;
								
							case 2: 
								
								String recorrido="Urbano";
								boolean escolar = false;
								

								
								try {
									
									int elRecorrido = Integer.parseInt(JOptionPane.showInputDialog("El recorrido es urbano o interubano?:\n 1.URBANO\n 2.INTERURBANO"));
										if(elRecorrido == 1){
											
											recorrido="Urbano";	
										
										}
										else if (elRecorrido == 2){
										
											recorrido="Interurbano";
											
										}
										
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
								
									int esEscolar = Integer.parseInt(JOptionPane.showInputDialog("El autob�s es escolar?:\n 1.SI\n 2.NO "));
										if(esEscolar == 1){
											
											escolar = true;	
										
										}
										else if (esEscolar == 2){
										
											escolar = false;
										}
										
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								Vehiculos.add(new Autobus(recorrido,escolar));
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								
								break;
							
							case 3: 
								
								repetir2 = false;
								break;
							}
						}while (repetir2);
								
					}
					break;
					
				case 3: 
														
					if(Vehiculo.getNumVehiculos()>(Vehiculo.maxVehiculos -1)){
						JOptionPane.showMessageDialog(null,"Parque lleno, no se pueden crear m�s veh�culos.");
					}else{
						
						do{
							String menu2 = "Seleccione opci�n para crear motocicleta:\n";
							menu2 = menu2 + "1. Fabricaci�n autom�tica\n";
							menu2 = menu2 + "2. Fabricaci�n indicando potencia del motor y si tiene maletero\n";
							menu2 = menu2 + "3. Atras\n";
							
							try {
								opcionString2 = JOptionPane.showInputDialog(menu2);

								opcion2 = Integer.parseInt(opcionString2);
							}
							catch (Exception E) {
								opcionString2 = "La opci�n elegida ha de ser num�rica";
								JOptionPane.showMessageDialog(null, opcionString2);
								repetir2 = true;
								opcion2 = -1;
							}
							
							switch (opcion2){
							
							case 1: 
													
								Vehiculos.add(new Motocicleta());
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								break;
								
							case 2: 
								
								int potenciaMotor=0;
								boolean tieneMaletero = false;
																
								try {
									
									potenciaMotor = Integer.parseInt(JOptionPane.showInputDialog("Introduce la potencia del motor:"));
																										
									}
								catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
									
									int maletero = Integer.parseInt(JOptionPane.showInputDialog("Tiene maletero?:\n 1.SI\n 2.NO "));
										if(maletero == 1){
											
											tieneMaletero = true;	
										
										}
										else if (maletero == 2){
										
											tieneMaletero = false;
											
										}
										
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								
								
								Vehiculos.add(new Motocicleta(potenciaMotor,tieneMaletero));
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								
								break;
							
							case 3: 
								
								repetir2 = false;
								break;
							}
						}while (repetir2);
								
					}
					break;
					
				case 4: 
					
					if(Vehiculo.getNumVehiculos()>(Vehiculo.maxVehiculos -1)){
						JOptionPane.showMessageDialog(null,"Parque lleno, no se pueden crear m�s veh�culos.");
					}else{
						
						do{
							String menu2 = "Seleccione opci�n para crear avioneta:\n";
							menu2 = menu2 + "1. Fabricaci�n autom�tica\n";
							menu2 = menu2 + "2. Fabricaci�n indicando aeropuerto y capacidad m�xima de carga\n";
							menu2 = menu2 + "3. Atras\n";
							
							try {
								opcionString2 = JOptionPane.showInputDialog(menu2);

								opcion2 = Integer.parseInt(opcionString2);
							}
							catch (Exception E) {
								opcionString2 = "La opci�n elegida ha de ser num�rica";
								JOptionPane.showMessageDialog(null, opcionString2);
								repetir2 = true;
								opcion2 = -1;
							}
							
							switch (opcion2){
							
							case 1: 
													
								Vehiculos.add(new Avioneta());
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								break;
								
							case 2: 
								
								int maximoKilos=0;
								String aeropuerto="";
								
								try {
									
									maximoKilos = Integer.parseInt(JOptionPane.showInputDialog("Introduce kilos m�ximos de carga:"));
																										}
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
									
									aeropuerto = JOptionPane.showInputDialog("Indica a que aeropuerto pertenece: ");
									
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								
								Vehiculos.add(new Avioneta(aeropuerto,maximoKilos));
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								
								break;
							
							case 3: 
								
								repetir2 = false;
								break;
							}
						}while (repetir2);
								
					}
					break;	
				
				case 5: 
										
					if(Vehiculo.getNumVehiculos()>(Vehiculo.maxVehiculos -1)){
						JOptionPane.showMessageDialog(null,"Parque lleno, no se pueden crear m�s veh�culos.");
					}else{
						
						do{
							String menu2 = "Seleccione opci�n para crear yate:\n";
							menu2 = menu2 + "1. Fabricaci�n autom�tica\n";
							menu2 = menu2 + "2. Fabricaci�n indicando si tiene cocina, numero de motores y metros de eslora\n";
							menu2 = menu2 + "3. Atras\n";
							
							try {
								opcionString2 = JOptionPane.showInputDialog(menu2);

								opcion2 = Integer.parseInt(opcionString2);
							}
							catch (Exception E) {
								opcionString2 = "La opci�n elegida ha de ser num�rica";
								JOptionPane.showMessageDialog(null, opcionString2);
								repetir2 = true;
								opcion2 = -1;
							}
							
							switch (opcion2){
							
							case 1: 
													
								Vehiculos.add(new Yate());
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								break;
								
							case 2: 
								
								int numeroMotores=0;
								int eslora=0;
								boolean tieneCocina = false;
								
								try {
									
									numeroMotores = Integer.parseInt(JOptionPane.showInputDialog("Introduce n�mero de motores:"));
																										}
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
									
									eslora = Integer.parseInt(JOptionPane.showInputDialog("Introduce metros de eslora: "));
																				
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								try {
								
									int cocina = Integer.parseInt(JOptionPane.showInputDialog("Tiene cocina?:\n 1.SI\n 2.NO "));
										if(cocina == 1){
											
											tieneCocina = true;	
										
										}
										else if (cocina == 2){
										
											tieneCocina = false;
										}
										
									}
								
									catch (Exception E) {
										JOptionPane.showMessageDialog(null, "Valor no v�lido, se crear� con el valor por defecto.");
									}
								
								Vehiculos.add(new Yate(tieneCocina,numeroMotores,eslora));
								auxiliar = Vehiculos.get(Vehiculos.size()-1);
								JOptionPane.showMessageDialog(null,auxiliar.toString());
								
								break;
							
							case 3: 
								
								repetir2 = false;
								break;
							}
						}while (repetir2);
								
					}
					break;
				
				case 6: 
					mostrarArray(Vehiculos);
					break;
				
				case 7: 
				
					repetir = false;
					break;		
				}
			
		}while (repetir);
		
	}
	
	public static int tipoVehiculo(Vehiculo vehiculo)
	{
		int clase = -1;
		
		if(vehiculo instanceof Coche){
			clase = 1;
		}else if(vehiculo instanceof Autobus){
			clase = 2;
		}else if(vehiculo instanceof Motocicleta){
			clase = 3;
		}else if(vehiculo instanceof Avioneta){
			clase = 4;
	    }else if(vehiculo instanceof Yate){
			clase = 5;
	    }
		return clase;
		
	}
	
	public static void mostrarArray(ArrayList<Vehiculo> vehiculos)
	{
		String datos = "";
		int claseDeVehiculo = -1;
		if(vehiculos.size() > 0)
		{
			
			for(Vehiculo v:vehiculos)
			{
				claseDeVehiculo = tipoVehiculo(v);
				
			   if(claseDeVehiculo == 1)
				{
					Coche coche = (Coche)v;
					datos = "";
					datos += " Datos Coche \n";
					datos += coche.toString();
					JOptionPane.showMessageDialog(null, datos);
					coche.circular();
				}else if(claseDeVehiculo == 2){
					Autobus bus = (Autobus)v;
					datos = "";
					datos += " Datos Autobus \n";
					datos += bus.toString();
					JOptionPane.showMessageDialog(null, datos);
					bus.circular();
				}else if(claseDeVehiculo == 3){
					Motocicleta moto = (Motocicleta)v;
					datos = "";
					datos += " Datos Motocicleta \n";
					datos += moto.toString();
					JOptionPane.showMessageDialog(null, datos);
					moto.circular();
				}else if(claseDeVehiculo == 4){
					Avioneta avion = (Avioneta)v;
					datos = "";
					datos += " Datos Avioneta \n";
					datos += avion.toString();
					JOptionPane.showMessageDialog(null, datos);
					avion.circular();
					avion.volar();
				}else if(claseDeVehiculo == 5){
					Yate barco = (Yate)v;
					datos = "";
					datos += " Datos Yate \n";
					datos += barco.toString();
					JOptionPane.showMessageDialog(null, datos);
					barco.navegar();
				}else{
					datos = "";
					datos += " Vehiculo incorrecto \n";
					JOptionPane.showMessageDialog(null, datos);
				}
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "No hay vehiculos para mostrar");
		}
	}
}
