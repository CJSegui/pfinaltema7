package pFinalTema7;
public abstract class Vehiculo {

	private String matricula;
	private String marca;
	private String modelo;
	private String color;
	private double kilometros;
	private int numPuertas;
	private int numPlazas;
	
	private static int numVehiculos=0;
	public static final int maxVehiculos = 5;
	
	public Vehiculo() {
		
		this.matricula = "1234ABC";
		this.marca = "Nissan";
		this.modelo = "Qashqai";
		this.color = "Blanco";
		this.kilometros = 0;
		this.numPuertas = 5;
		this.numPlazas = 5;
		Vehiculo.setNumVehiculos(Vehiculo.getNumVehiculos() + 1);
	}
	
	public Vehiculo(String matricula, String marca, String modelo, String color, double kilometros, int numPuertas,int numPlazas) {
		
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;
		this.kilometros = kilometros;
		this.numPuertas = numPuertas;
		this.numPlazas = numPlazas;
		Vehiculo.setNumVehiculos(Vehiculo.getNumVehiculos() + 1);
	}
	
	public abstract void arrancar();
	public abstract void acelerar();
	public abstract void frenar();
	
	@Override
	public String toString(){
		
		String caracteristicas = "";
		
		caracteristicas = caracteristicas + "La matr�cula del Veh�culo es: " + matricula +"\n";
		caracteristicas = caracteristicas + "La marca del Veh�culo es: " + marca +"\n";
		caracteristicas = caracteristicas + "El modelo del Veh�culo es: " + modelo +"\n";
		caracteristicas = caracteristicas + "El color del Veh�culo es: " +  color +"\n";
		caracteristicas = caracteristicas + "Los kilometros del Veh�culo son: " + kilometros +"\n";
		caracteristicas = caracteristicas + "El n�mero de puertas del Veh�culo es: " + numPuertas +"\n";
		caracteristicas = caracteristicas + "El n�mero de plazas del Veh�culo es: " + numPlazas +"\n";
				
		return caracteristicas;
	}
	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getKilometros() {
		return kilometros;
	}
	public void setKilometros(double kilometros) {
		this.kilometros = kilometros;
	}
	public int getNumPuertas() {
		return numPuertas;
	}
	public void setNumPuertas(int numPuertas) {
		this.numPuertas = numPuertas;
	}
	public int getNumPlazas() {
		return numPlazas;
	}
	public void setNumPlazas(int numPlazas) {
		this.numPlazas = numPlazas;
	}

	public static int getNumVehiculos() {
		return numVehiculos;
	}

	public static void setNumVehiculos(int numVehiculos) {
		Vehiculo.numVehiculos = numVehiculos;
	}

	
}
