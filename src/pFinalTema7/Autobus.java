package pFinalTema7;

import javax.swing.JOptionPane;

public class Autobus extends Vehiculo implements PuedeCircular{

	private String tipoRecorrido;
	private boolean esEscolar;
	
		
	public Autobus() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Autobus(String tipoRecorrido, boolean esEscolar) {
		super();
		this.tipoRecorrido = tipoRecorrido;
		this.esEscolar = esEscolar;
	}
	@Override
	public String toString(){
		
		String caracteristicas = super.toString();
		
		caracteristicas = caracteristicas + "El tipo de recorrido del autob�s es: " + tipoRecorrido +"\n";
				
		if (esEscolar){
			caracteristicas = caracteristicas + "El autob�s es escolar\n";
		}
		else{
			caracteristicas = caracteristicas + "El autob�s no es escolar\n";
		}
						
		return caracteristicas;
		
	}
	
	
	public String getTipoRecorrido() {
		return tipoRecorrido;
	}
	public void setTipoRecorrido(String tipoRecorrido) {
		this.tipoRecorrido = tipoRecorrido;
	}
	public boolean isEsEscolar() {
		return esEscolar;
	}
	public void setEsEscolar(boolean esEscolar) {
		this.esEscolar = esEscolar;
	}
	public void aparcar(){
		JOptionPane.showMessageDialog(null,"El autob�s est� aparcado.");
	}
	public void abrirPuertas(){
		JOptionPane.showMessageDialog(null,"Las puertas est�n abiertas.");	
	}
	@Override
	public void circular() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"Esto es un autob�s y los autobuses pueden circular por carreteras, autov�as y autopistas.");
	}
	@Override
	public void arrancar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El autob�s ha arrancado.");
	}
	@Override
	public void acelerar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El autob�s ha acelerado.");
	}
	@Override
	public void frenar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El autob�s ha frenado.");
	}
}
