package pFinalTema7;

import javax.swing.JOptionPane;

public class Coche extends Vehiculo implements  PuedeCircular {

	private int numAirbags;
	private boolean techoSolar;
	private boolean tieneRadio;
		
	public Coche() {
		super();
		this.numAirbags = 0;
		this.techoSolar = false;
		this.tieneRadio = false;
		
		
	}
	public Coche(int numAirbags, boolean techoSolar, boolean tieneRadio) {
		super();
		this.numAirbags = numAirbags;
		this.techoSolar = techoSolar;
		this.tieneRadio = tieneRadio;
	}
	@Override
	public String toString(){
		
		String caracteristicas = super.toString();
		
		caracteristicas = caracteristicas + "El numero de airbags es: " + numAirbags +"\n";
		
		if (techoSolar){
			caracteristicas = caracteristicas + "El coche tiene techo solar\n";
		}
		else{
			caracteristicas = caracteristicas + "El coche no tiene techo solar\n";
		}
			
		if (tieneRadio){
			caracteristicas = caracteristicas + "El coche tiene radio\n";
		}
		else{
			caracteristicas = caracteristicas + "El coche no tiene radio\n";
		}
		
		return caracteristicas;
		
	}
	
	public int getNumAirbags() {
		return numAirbags;
	}
	public void setNumAirbags(int numAirbags) {
		this.numAirbags = numAirbags;
	}
	public boolean isTechoSolar() {
		return techoSolar;
	}
	public void setTechoSolar(boolean techoSolar) {
		this.techoSolar = techoSolar;
	}
	public boolean isTieneRadio() {
		return tieneRadio;
	}
	public void setTieneRadio(boolean tieneRadio) {
		this.tieneRadio = tieneRadio;
	}
	public void aparcar(){
		JOptionPane.showMessageDialog(null,"El coche est� aparcado.");
	}
	public void tunear(String color){
			
		this.setKilometros(0);
		this.setColor(color);
		if(!this.isTechoSolar()){
			this.setTechoSolar(true);
						
		}
		JOptionPane.showMessageDialog(null,"El cocehe se ha tuneado.");
	}
	@Override
	public void circular() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"Esto es un coche y los coches puede circular por carreteras, autov�as y autopistas.");
	}
	@Override
	public void arrancar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El coche ha arrancado.");
	}
	@Override
	public void acelerar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El coche ha acelerado.");
	}
	@Override
	public void frenar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"El coche ha frenado.");
	}
	
}
