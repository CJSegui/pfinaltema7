package pFinalTema7;

import javax.swing.JOptionPane;

public class Avioneta extends Vehiculo implements PuedeVolar, PuedeCircular {

	private String aeropuerto;
	private int maxKg;
	
		public Avioneta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Avioneta(String aeropuerto, int maxKg) {
		super();
		this.aeropuerto = aeropuerto;
		this.maxKg = maxKg;
	}
	@Override
	public String toString(){
		
		String caracteristicas = super.toString();
		
		caracteristicas = caracteristicas + "La avioneta pertenece al aeropuerto: " + aeropuerto +"\n";
				
		caracteristicas = caracteristicas + "Los kilos m�ximos de carga de la avioneta son:" + maxKg + "\n";
						
		return caracteristicas;
		
	}
	
	
	public String getAeropuerto() {
		return aeropuerto;
	}
	public void setAeropuerto(String aeropuerto) {
		this.aeropuerto = aeropuerto;
	}
	public int getMaxKg() {
		return maxKg;
	}
	public void setMaxKg(int maxKg) {
		this.maxKg = maxKg;
	}
	public void despegar(){
		JOptionPane.showMessageDialog(null,"La avioneta est� despegando.");
	}
	public void aterrizar(){
		JOptionPane.showMessageDialog(null,"La avioneta est� aterrizando.");
	}
	public void circular() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"Esto es una avioneta y las avionetas s�lo pueden circular dentro de los aeropuertos.");
	}
	@Override
	public void arrancar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La avioneta ha arrancado.");
	}
	@Override
	public void acelerar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La avioneta ha acelerado.");
	}
	@Override
	public void frenar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La avioneta ha frenado.");
	}
	@Override
	public void volar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null,"La avioneta est� volando.");
	}
}

